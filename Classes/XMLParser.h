//
//  XMLParser.h
//  DeskTop
//
//  Created by Acmesoftcn on 12/30/11.
//  Copyright 2011 XiaoLei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Group.h"

@interface XMLParser : NSObject<NSXMLParserDelegate> {
	NSXMLParser *m_parser;
	NSMutableArray *groups;
	NSString *m_strCurrentElement;
//	Card *card;
	NSMutableDictionary *card;
	Group *group;
}
+ (XMLParser *)GetInstance;
- (NSMutableArray*) GetArrayByPaserXML;
@end
