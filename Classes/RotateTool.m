//
//  RotateTool.m
//  DeskTop
//
//  Created by Acmesoftcn on 12/30/11.
//  Copyright 2011 XiaoLei. All rights reserved.
//

#import "RotateTool.h"


@implementation RotateTool
@synthesize rv,rv1,rv2;
-(void)rotateView:(UIView*)v{
	self.rv=v;
	CATransform3D landscapeTransform;
	float flp= M_PI/2;
	landscapeTransform = CATransform3DMakeRotation (flp, 1, 0, 0);
	//v.layer.transform = landscapeTransform;
    CABasicAnimation *animation;
	animation=[CABasicAnimation animationWithKeyPath:@"transform"];
	animation.toValue=[NSValue valueWithCATransform3D:landscapeTransform];
	animation.duration=0.35;
	animation.autoreverses=YES;
	animation.cumulative=YES;
	animation.repeatCount=1;
	animation.delegate=self;
	[v.layer addAnimation:animation forKey:nil];
	
	
}
-(void)rotateFromView:(UIView*)v1 toView:(UIView*)v2 inSuperView:(UIView*)v{
	self.rv=v;
	self.rv1=v1;
	self.rv2=v2;
    CATransform3D landscapeTransform;
	float flp= M_PI/2;
	landscapeTransform = CATransform3DMakeRotation (flp, 1, 0, 0);
	//v.layer.transform = landscapeTransform;
    CABasicAnimation *animation;
	animation=[CABasicAnimation animationWithKeyPath:@"transform"];
	animation.toValue=[NSValue valueWithCATransform3D:landscapeTransform];
	animation.duration=0.5;
	animation.autoreverses=YES;
	animation.cumulative=YES;
	animation.repeatCount=1;
	animation.delegate=self;
	[v.layer addAnimation:animation forKey:nil];
    
}
- (void)animationDidStart:(CAAnimation *)anim{
	//the time interval(0.26) is a bit more than half the animation dutation(0.5) 
	[NSTimer scheduledTimerWithTimeInterval:0.26 target:self selector:@selector(showOrHidden) userInfo:nil repeats:NO];
}
-(void)showOrHidden{
   for (UIView *sub in rv.subviews) {
		if (sub.tag==2) {
			if (sub.hidden) {
				sub.hidden=NO;
			}else {
				sub.hidden=YES;

			}
			
		}
	}
}
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
	
	
	
	//[self.rv bringSubviewToFront:rv2];
//	[self.rv sendSubviewToBack:rv1];


}

@end
