//
//  DeskTopViewController.m
//  DeskTop
//
//  Created by Acmesoftcn on 12/30/11.
//  Copyright 2011 XiaoLei. All rights reserved.
//

#import "DeskTopViewController.h"
#import "defines.h"

@implementation DeskTopViewController
@synthesize groups,tb,study,grpsV,studyRst,showCards;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	[self testXMLParser];
	[self initSubViews];
	[self addUI];
    
    //SH
    UIBarButtonItem *showM = [[UIBarButtonItem alloc] initWithTitle:@"Start Session"
                                                              style:UIBarButtonItemStylePlain
                                                             target:self
                                                             action:@selector(studyAll)];
    self.navigationItem.rightBarButtonItem = showM;

}

-(void)viewWillAppear:(BOOL)animated{
    if (self.studyRst) {
        [studyRst readRecentRecords];
        [studyRst showRightRecords];
    }
    self.navigationController.navigationBarHidden=NO;
}

-(void)initSubViews {
    CGRect frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height);
	self.grpsV = [[Groups alloc]initWithFrame:frame];
    //[self.grpsV setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
	self.studyRst = [[StudyResult alloc]initWithFrame:frame];
//    [self.studyRst setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
	self.showCards = [[ShowStudyCards alloc]init];
	self.study=[[Study alloc]init];
//	grpsV.groups=self.groups;
	[self.view addSubview:grpsV];
	[self.view addSubview:studyRst];
    
    [self.grpsV setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:self.grpsV attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *leadingConstraint = [NSLayoutConstraint constraintWithItem:self.grpsV attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *trailingConstraint = [NSLayoutConstraint constraintWithItem:self.grpsV attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    [self.view addConstraints:@[topConstraint, leadingConstraint, trailingConstraint]];
    

    [self.studyRst setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *studyTopConstraint = [NSLayoutConstraint constraintWithItem:self.studyRst attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *studyLeadingConstraint = [NSLayoutConstraint constraintWithItem:self.studyRst attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *studyTrailingConstraint = [NSLayoutConstraint constraintWithItem:self.studyRst attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    [self.view addConstraints:@[studyTopConstraint, studyLeadingConstraint, studyTrailingConstraint]];
    
	[self.view bringSubviewToFront:studyRst];
	self.navigationItem.title=@"History";
	
}

-(void)addUI{
	self.tb = [[UITabBar alloc] initWithFrame:CGRectMake(0 , self.view.frame.size.height, self.view.frame.size.width, tabBarHeight)];
//    [self.tb setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
	tb.delegate=self;
	NSMutableArray *itms=[NSMutableArray arrayWithCapacity:0];
	itemRnt=[[UITabBarItem alloc]initWithTitle:@" " image:nil tag:0];
	itemGps=[[UITabBarItem alloc]initWithTitle:@" " image:nil tag:1];
    
    [itms addObject:itemRnt];
    [itms addObject:itemGps];
    tb.items=itms;

	btnReview=[UIButton buttonWithType:UIButtonTypeSystem];
	btnReview.frame=CGRectMake(0, 0, 160, tabBarHeight);
	[btnReview setTitle:@"History" forState:UIControlStateNormal];
    [btnReview setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btnReview addTarget:self action:@selector(review) forControlEvents:UIControlEventTouchUpInside];
	
	AllCards=[UIButton buttonWithType:UIButtonTypeSystem];
	AllCards.frame=CGRectMake(160, 0, 160, tabBarHeight);
	[AllCards setTitle:@"Cards" forState:UIControlStateNormal];
    [AllCards setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [AllCards addTarget:self action:@selector(allcards) forControlEvents:UIControlEventTouchUpInside];
    
 
	[tb addSubview:btnReview];
	[tb addSubview:AllCards];
    [btnReview setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *btnReviewTopConstraint = [NSLayoutConstraint constraintWithItem:tb attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:btnReview attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *btnReviewLeadingConstraint = [NSLayoutConstraint constraintWithItem:tb attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:btnReview attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *btnReviewBottomConstraint = [NSLayoutConstraint constraintWithItem:tb attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:btnReview attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    [tb addConstraints:@[btnReviewTopConstraint, btnReviewLeadingConstraint, btnReviewBottomConstraint]];
    
    [AllCards setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *AllCardsTopConstraint = [NSLayoutConstraint constraintWithItem:tb attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:AllCards attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *AllCardsTrailingConstraint = [NSLayoutConstraint constraintWithItem:tb attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:AllCards attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    NSLayoutConstraint *AllCardsLeadingConstraint = [NSLayoutConstraint constraintWithItem:btnReview attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:AllCards attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *AllCardsBottomConstraint = [NSLayoutConstraint constraintWithItem:tb attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:AllCards attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    NSLayoutConstraint *AllCardsEqualWidthConstraint = [NSLayoutConstraint constraintWithItem:btnReview attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:AllCards attribute:NSLayoutAttributeWidth multiplier:1 constant:0];
    [tb addConstraints:@[AllCardsTopConstraint, AllCardsTrailingConstraint, AllCardsBottomConstraint, AllCardsEqualWidthConstraint, AllCardsLeadingConstraint]];
    
	//[tb sendSubviewToBack:btnReview];
	//[tb sendSubviewToBack:AllCards];
	[tb setSelectedItem:itemRnt];
	[self.view addSubview:tb];
    
    [self.tb setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:self.grpsV attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.tb attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *studyRsttopConstraint = [NSLayoutConstraint constraintWithItem:self.studyRst attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.tb attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:self.tb attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:40];
    NSLayoutConstraint *leadingConstraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.tb attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *trailingConstraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.tb attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:self.tb attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    [self.view addConstraints:@[studyRsttopConstraint, topConstraint, bottomConstraint, leadingConstraint, trailingConstraint, heightConstraint]];
}
-(void)review{
    [tb setSelectedItem:itemRnt];
    [self tabBar:tb didSelectItem:itemRnt];
    [btnReview setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [AllCards setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
}
-(void)allcards{

    [tb setSelectedItem:itemGps];
    [self tabBar:tb didSelectItem:itemGps];

    [btnReview setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [AllCards setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];

}
-(void)testClock{
	//Clock *clk=[[Clock alloc] initWithDatePickerForView:self.view];

}

//- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
//    CGRect viewFrame = CGRectMake(0, 0, size.width, size.height);
//    self.grpsV.frame = viewFrame;
//    self.studyRst.frame = viewFrame;
//    self.tb.frame = CGRectMake(0, size.height, size.width, tabBarHeight);
//}

-(void)testXMLParser{
	self.groups=allGroups;
}


-(void)cardsTabActivated
{
    [self.view bringSubviewToFront:grpsV];
    [self.view bringSubviewToFront:self.tb];
    self.navigationItem.title=@"Cards";

}

-(void)tabBar:(UITabBar *)tb didSelectItem:(UITabBarItem *)item{
   switch (item.tag) {
	   case 0:
		   [self.view bringSubviewToFront:studyRst];
		   [self.view bringSubviewToFront:self.tb];
		   self.navigationItem.title=@"History";
		   //self.navigationItem.rightBarButtonItem=nil;
		   break;
	   case 1: {
		   [self.view bringSubviewToFront:grpsV];
		   [self.view bringSubviewToFront:self.tb];
		   self.navigationItem.title=@"Cards";
           /*
		   UIBarButtonItem *showM = [[UIBarButtonItem alloc] initWithTitle:@"Start Session" 
																	 style:UIBarButtonItemStylePlain
                                                                    target:self 
																	action:@selector(studyAll)];
		   self.navigationItem.rightBarButtonItem = showM;*/
		   break;
	   }
	   default:
		   self.navigationItem.title=@"";
		   break;
   }
}
-(void)studyAll{

    [self.grpsV studyAll];
    //[self showCardsOfGroup:self.grpsV.group];
}
// Override to allow orientations other than the default portrait orientation.
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    // Return YES for supported orientations
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
//}

-(void)showCardsOfGroup:(Group*)group{
	showCards.navigationItem.title=group.groupName;
	showCards.cards=group.cards;
	showCards.cardsgroup=self.groups;
//	showCards.rightCards=[NSMutableArray arrayWithArray:group.cards];
	[showCards.cardTbl reloadData];
	[self.navigationController pushViewController:showCards animated:YES];
    
}
-(void)studyCards:(NSMutableArray*)cards{
	self.study=[[Study alloc]init];
	study.cards=cards;
	[self.navigationController pushViewController:study animated:YES];

}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

@end
