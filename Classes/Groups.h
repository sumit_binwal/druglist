//
//  Groups.h
//  Parmedic Drug List
//
//  Created by Acmesoftcn on 1/7/12.
//  Copyright 2012 XiaoLei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Group.h"
#import "Study.h"
@interface Groups : UIView <UITableViewDelegate,UITableViewDataSource>{
	UITableView *groupTbl;
	NSMutableArray *groups;
	Group *group;
	NSMutableDictionary *card;
	Study *study;
}
@property(nonatomic,strong)NSMutableArray *groups;
@property(nonatomic,strong)Group *group;
@property(nonatomic,strong)NSMutableDictionary *card;
@property(nonatomic,strong)Study *study;
-(void)studyAll;
@end
