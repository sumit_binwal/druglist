//
//  RecentStudy.m
//  DeskTop
//
//  Created by Acmesoftcn on 1/5/12.
//  Copyright 2012 XiaoLei. All rights reserved.
//

#import "RecentStudy.h"
#import "Card.h"

@implementation RecentStudy
@synthesize studycard,recordMaxNum,storeRight,storeWrong;

- (id)initWithMaxRecordNum:(int)num{
	if (self=[super init]) {
		self.recordMaxNum=num;
	}
	return self;
}

- (id)initWithDefaultRecordNum{
	storeRight=allRightsGroup;
	storeWrong=allWrongsGroup;
	if (self=[super init]) {
		self.recordMaxNum=50;
	}
	return self;
}

- (void)addCard:(NSMutableDictionary*)card {

	self.studycard = card;
}

- (void)storeRightRecent{
    if (self.studycard) {
        BOOL y = [storeRight containsObject:self.studycard];
        if (!y) {
            [storeRight addObject:self.studycard];
            [storeRight writeToFile:[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"right.plist"] atomically:YES];
        }
        BOOL x = [storeWrong containsObject:self.studycard];
        if (x) {
            [storeWrong removeObject:self.studycard];
            [storeWrong writeToFile:[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"wrong.plist"] atomically:YES];
        }
    }
}

- (void)storeWrongRecent{
	BOOL y = [storeRight containsObject:self.studycard];
	if (y) {
		
        [storeRight removeObject:self.studycard];
        [storeRight writeToFile:[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"right.plist"] atomically:YES];
	}
	BOOL x = [storeWrong containsObject:self.studycard];
	if (!x) {
        [storeWrong addObject:self.studycard];
        [storeWrong writeToFile:[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"wrong.plist"] atomically:YES];
		
	}
}

- (void)clear{
	NSFileManager *fileManager = [NSFileManager defaultManager];
	[fileManager createFileAtPath:[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"right.plist"] contents:nil attributes:nil];
	[fileManager createFileAtPath:[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"wrong.plist"] contents:nil attributes:nil];

}
@end
