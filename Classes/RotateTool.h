//
//  RotateTool.h
//  DeskTop
//
//  Created by Acmesoftcn on 12/30/11.
//  Copyright 2011 XiaoLei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CALayer.h>
@interface RotateTool : NSObject {
	UIView *rv;
	UIView *rv1;
	UIView *rv2;
}
@property(nonatomic,strong)UIView *rv;
@property(nonatomic,strong)UIView *rv2;
@property(nonatomic,strong)UIView *rv1;
-(void)rotateView:(UIView*)v;
-(void)rotateFromView:(UIView*)v1 toView:(UIView*)v2 inSuperView:(UIView*)v;
@end
