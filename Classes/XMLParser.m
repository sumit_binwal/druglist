//
//  XMLParser.m
//  DeskTop
//
//  Created by Acmesoftcn on 12/30/11.
//  Copyright 2011 XiaoLei. All rights reserved.
//

#import "XMLParser.h"


@implementation XMLParser

static XMLParser *instance;  //单例对象 
BOOL flag = YES;                               //是否为一个节点 重复的赋值 
NSString* m_strCurrentElement;  //读到的当前节点的名 

+ (XMLParser *)GetInstance { 
	@synchronized(self) { 
		if (instance == nil) { 
			instance = [[self alloc] init]; 
		} 
	} 
	return instance; 
} 

- (NSMutableArray*) GetArrayByPaserXML {	
	NSString* path = path = [[NSBundle mainBundle] pathForResource:@"cards" ofType:@"xml"];
	NSFileHandle* file = [NSFileHandle fileHandleForReadingAtPath:path]; 
	NSData* data = [file readDataToEndOfFile]; 
	[file closeFile]; 
	
	m_parser = [[NSXMLParser alloc] initWithData:data]; 
	
	//设置该类本身为代理类 
	[m_parser setDelegate:self];
    [m_parser parse];
    
    return groups;
} 

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName 
  namespaceURI:(NSString *) namespaceURI qualifiedName:(NSString *)qName 
	attributes: (NSDictionary *)attributeDict { 
	
	//读到开始标签 flag = YES 
	flag = YES; 
	
	// 纪录当前解析的节点 
	m_strCurrentElement = elementName; 
	
	////读到xml的跟结点的开始标签 
	if ([elementName isEqualToString:@"card"]) { 
		card=[NSMutableDictionary new];
		[card setObject:[attributeDict objectForKey:@"term"] forKey:@"term"];
		[card setObject:[attributeDict objectForKey:@"definition"] forKey:@"definition"];
		[group.cards addObject:card];
		card=nil;
	} else if ([elementName isEqualToString:@"group"]) {
		
		if (group) {
			[groups addObject:group];
			group=nil;
		}

	}else if ([elementName isEqualToString:@"card-groups"]) {
		groups=[[NSMutableArray alloc]initWithCapacity:0];
	}

} 

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {

	if (flag) { 
		if ([m_strCurrentElement isEqualToString:@"group"]) {
			group=[[Group alloc]initWithName:string];
		}
	
	} 
} 

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName { 
	//读到结束标签 flag = NO 
	flag = NO; 
   if ([elementName isEqualToString:@"card-groups"]) {
	   [groups addObject:group];
   }
	
} 

- (void)parserDidStartDocument:(NSXMLParser *)parser { 
	NSLog(@"stard to parser"); 
} 

- (void)parserDidEndDocument:(NSXMLParser *)parser { 
	NSLog(@"parser finished"); 
} 




@end
