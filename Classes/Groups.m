//
//  Groups.m
//  Parmedic Drug List
//
//  Created by Acmesoftcn on 1/7/12.
//  Copyright 2012 XiaoLei. All rights reserved.
//

#import "Groups.h"
#import "DeskTopAppDelegate.h"

@implementation Groups

@synthesize groups,group,card,study;
- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
		self.groups = allGroups;
        self.backgroundColor=[UIColor grayColor];
		self.study=[Study new];
		groupTbl=[[UITableView alloc]initWithFrame:self.frame style:UITableViewStyleGrouped];
        [groupTbl setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
		groupTbl.delegate=self;
		groupTbl.dataSource=self;
//		groupTbl.style=UITableViewStyleGrouped;
		//SH groupTbl.backgroundColor=[UIColor grayColor];
		//SH groupTbl.separatorColor=[UIColor whiteColor];
		[self addSubview:groupTbl];
    }
    return self;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
	self.group=[self.groups objectAtIndex:0];
    return [self.groups count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [[[groups objectAtIndex:section] cards] count];
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
	
	static NSString *GroupTableIdentifier = @"GroupTableIdentifier";
	NSInteger sections = indexPath.section;
    NSInteger index=indexPath.row;
	self.card=[[[self.groups objectAtIndex:sections] cards] objectAtIndex:index];
    UITableViewCell *cell;
	cell= [tableView dequeueReusableCellWithIdentifier:GroupTableIdentifier];
	
	if (!cell) {
		//cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:GroupTableIdentifier];
		//SH cell.backgroundColor=[UIColor grayColor];
		//SH [cell.textLabel setTextColor:[UIColor whiteColor]];
        cell.textLabel.font = [UIFont systemFontOfSize:12.5];
        cell.textLabel.numberOfLines = 2;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	}

    cell.textLabel.text=[card objectForKey:@"term"];

    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	NSString *headerString = [NSString stringWithFormat:@"%@",[[self.groups objectAtIndex:section] groupName]];
	return headerString;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	NSMutableArray *cards=[NSMutableArray arrayWithCapacity:0];
	NSInteger section=indexPath.section;
	NSInteger index=indexPath.row;
	self.card=[[[self.groups objectAtIndex:section] cards] objectAtIndex:index];
    [cards addObject:card];
	DeskTopAppDelegate* delegate=(DeskTopAppDelegate*)[[UIApplication sharedApplication] delegate];
	[delegate studyCards:cards];
	
}

-(void)studyAll{
	DeskTopAppDelegate* delegate=(DeskTopAppDelegate*)[[UIApplication sharedApplication] delegate];
	[delegate showCardsOfGroup:self.group];

}


@end
