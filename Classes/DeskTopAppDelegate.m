//
//  DeskTopAppDelegate.m
//  DeskTop
//
//  Created by Acmesoftcn on 12/30/11.
//  Copyright 2011 XiaoLei. All rights reserved.
//

#import "DeskTopAppDelegate.h"
#import "DeskTopViewController.h"
#import "HomeViewController.h"
#import <StoreKit/StoreKit.h>


CGRect windowFrame;
CGFloat windowWidth;
CGFloat windowHeight;
CGFloat tabBarHeight;
CGFloat statusBarHeight;
CGFloat navigationBarHeight;

@interface DeskTopAppDelegate ()

@property (nonatomic, strong) NSTimer *storeReviewTimer;

@end

@implementation DeskTopAppDelegate

@synthesize window;
@synthesize viewController;
@synthesize groups,rightsGroup,wrongsGroup;

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self initialize];

    [self startTimerForStoreReview];
    
	rightsGroup=[NSMutableArray arrayWithCapacity:0];
	wrongsGroup=[NSMutableArray arrayWithCapacity:0];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *DocumentsDirectory1 = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"right.plist"];
	if (![fileManager fileExistsAtPath:DocumentsDirectory1]) {
		[fileManager createFileAtPath:DocumentsDirectory1 contents:nil attributes:nil];
	}else {
		[self.rightsGroup addObjectsFromArray:[[NSArray alloc] initWithContentsOfFile:DocumentsDirectory1]];
	}

	NSString *DocumentsDirectory2 = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"wrong.plist"];
	if (![fileManager fileExistsAtPath:DocumentsDirectory2]) {
		[fileManager createFileAtPath:DocumentsDirectory2 contents:nil attributes:nil];
	}else {
		[self.wrongsGroup addObjectsFromArray:[[NSArray alloc] initWithContentsOfFile:DocumentsDirectory2]];
	}

    XMLParser *xmlP=[XMLParser GetInstance];
	self.groups=[xmlP GetArrayByPaserXML];
    
    self.homeCtrl = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
	self.navigationCtrl = [[CustomNavigationViewController alloc] initWithRootViewController:self.homeCtrl];
	self.navigationCtrl.navigationBar.tintColor=[UIColor blackColor];
    self.navigationCtrl.navigationBar.translucent = false;
    //[self.window addSubview:nav.view];
    self.window.rootViewController = self.navigationCtrl;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)initialize
{
    windowFrame = [[UIScreen mainScreen] bounds];
    windowWidth = windowFrame.size.width;
    windowHeight = windowFrame.size.height;
    tabBarHeight = 40;
    statusBarHeight = 20.0;
    navigationBarHeight = 0;
}

- (void)startTimerForStoreReview {
    self.storeReviewTimer = [NSTimer scheduledTimerWithTimeInterval:(3 * 60) repeats:YES block:^(NSTimer * _Nonnull timer) {
        if([SKStoreReviewController class]){
            [SKStoreReviewController requestReview];
        }
    }];
}

-(void)showCardsOfGroup:(Group*)group{
    [viewController showCardsOfGroup:group];

}
-(void)studyCards:(NSMutableArray*)cards{

    if (viewController && [viewController isKindOfClass:[DeskTopViewController class]]) {
        [viewController studyCards:cards];
    }
}
- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}




@end
