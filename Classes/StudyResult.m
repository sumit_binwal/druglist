//
//  StudyResult.m
//  Parmedic Drug List
//
//  Created by Acmesoftcn on 1/7/12.
//  Copyright 2012 XiaoLei. All rights reserved.
//

#import "StudyResult.h"
#import "defines.h"

@implementation StudyResult
@synthesize rstSdy,rightRecorts,wrongRecorts,tableResult;

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor=[UIColor blackColor];
        self.backgroundColor=[UIColor whiteColor];
        self.rstSdy=[[RecentStudy alloc]initWithDefaultRecordNum];
//		self.rightRecorts=[[NSMutableArray alloc]initWithCapacity:0];
//		self.wrongRecorts=[[NSMutableArray alloc]initWithCapacity:0];
		[self addSubviews];
    }
    return self;
}

-(void)addSubviews{
	UIButton *clearAll=[UIButton new];
	//clearAll.frame=CGRectMake(10, 110, 300, 33);
    clearAll.frame=CGRectMake(10, 10, 300, 33);
	[clearAll setBackgroundImage:[UIImage imageNamed:@"longgray.png"] forState:UIControlStateNormal];
//    [clearAll setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
	[clearAll setTitle:@"Clear Study History" forState:UIControlStateNormal];
	clearAll.titleLabel.textAlignment=NSTextAlignmentLeft;
	clearAll.titleLabel.font=[UIFont systemFontOfSize:18];
	[clearAll setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[clearAll addTarget:self action:@selector(clearHistory) forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:clearAll];
    
    [clearAll setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:clearAll attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:10];
    NSLayoutConstraint *leadingConstraint = [NSLayoutConstraint constraintWithItem:clearAll attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:10];
    NSLayoutConstraint *trailingConstraint = [NSLayoutConstraint constraintWithItem:clearAll attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1 constant:-10];
    [self addConstraints:@[topConstraint, leadingConstraint, trailingConstraint]];
    
    CGFloat width = 300;
    CGFloat originX = (self.frame.size.width - width) / 2;
    leftRightView = [[UIView alloc] initWithFrame:CGRectMake(originX, 53.0, 300, 35.0)];
    [leftRightView setBackgroundColor:[UIColor clearColor]];
    [leftRightView setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin];
    [self addSubview:leftRightView];
    
    [leftRightView setTranslatesAutoresizingMaskIntoConstraints:NO];
//    NSLayoutConstraint *leftRightViewCenterXConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:leftRightView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
//    NSLayoutConstraint *leftRightViewWidthConstraint = [NSLayoutConstraint constraintWithItem:leftRightView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:300];
    NSLayoutConstraint *leftRightViewLeadingConstraint = [NSLayoutConstraint constraintWithItem:clearAll attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:leftRightView attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *leftRightViewTrailingConstraint = [NSLayoutConstraint constraintWithItem:clearAll attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:leftRightView attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    NSLayoutConstraint *leftRightViewTopConstraint = [NSLayoutConstraint constraintWithItem:clearAll attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:leftRightView attribute:NSLayoutAttributeTop multiplier:1 constant: -10];
    NSLayoutConstraint *leftRightViewHeightConstraint = [NSLayoutConstraint constraintWithItem:leftRightView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant: 38];
    [self addConstraints:@[leftRightViewLeadingConstraint, leftRightViewTopConstraint, leftRightViewTrailingConstraint, leftRightViewHeightConstraint]];
    
    CGFloat insideBtnOriginX = 10;
	left=[UIButton new];
	//left.frame=CGRectMake(10, 160, 150, 33);
    left.frame=CGRectMake(0, 0, (clearAll.frame.size.width-10)/2, leftRightView.frame.size.height);
	[left setBackgroundImage:[UIImage imageNamed:@"leftblue.png"] forState:UIControlStateNormal];
    [left setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
	[left setTitle:@"Answered  Correctly" forState:UIControlStateNormal];
	left.titleLabel.font=[UIFont systemFontOfSize:14];
	[left setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[left addTarget:self action:@selector(showRightRecords) forControlEvents:UIControlEventTouchUpInside];
	[leftRightView addSubview:left];
    insideBtnOriginX += left.frame.size.width-1;
    
    [left setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *leftLeadingConstraint = [NSLayoutConstraint constraintWithItem:left attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:leftRightView attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *leftTopConstraint = [NSLayoutConstraint constraintWithItem:left attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:leftRightView attribute:NSLayoutAttributeTop multiplier:1 constant: 0];
    self.leftHeightConstraint = [NSLayoutConstraint constraintWithItem:left attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant: 35];
    [leftRightView addConstraints:@[leftLeadingConstraint, leftTopConstraint, self.leftHeightConstraint]];
	
	right=[UIButton new];
	//right.frame=CGRectMake(162, 160, 150, 31);
    right.frame=CGRectMake(insideBtnOriginX, 0, (clearAll.frame.size.width-10)/2, leftRightView.frame.size.height);
	[right setBackgroundImage:[UIImage imageNamed:@"redrightPop.png"] forState:UIControlStateNormal];
    [right setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
	[right setTitle:@"Answered Incorrectly" forState:UIControlStateNormal];
	right.titleLabel.font=[UIFont systemFontOfSize:14];
	[right setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[right addTarget:self action:@selector(showWrongRecords) forControlEvents:UIControlEventTouchUpInside];
	[leftRightView addSubview:right];
    
    [right setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *rightTrailingConstraint = [NSLayoutConstraint constraintWithItem:right attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:leftRightView attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    NSLayoutConstraint *rightLeadingConstraint = [NSLayoutConstraint constraintWithItem:right attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:left attribute:NSLayoutAttributeTrailing multiplier:1 constant:2];
    NSLayoutConstraint *rightTopConstraint = [NSLayoutConstraint constraintWithItem:right attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:leftRightView attribute:NSLayoutAttributeTop multiplier:1 constant: 0];
    self.rightHeightConstraint = [NSLayoutConstraint constraintWithItem:right attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant: 35];
    NSLayoutConstraint *leftRightEqualConstraint = [NSLayoutConstraint constraintWithItem:left attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:right attribute:NSLayoutAttributeWidth multiplier:1 constant: 0];
    [leftRightView addConstraints:@[rightLeadingConstraint, rightTrailingConstraint, rightTopConstraint, self.rightHeightConstraint, leftRightEqualConstraint]];
    
    self.tableResult = [[UITableView alloc]initWithFrame:CGRectMake(0, 90, self.frame.size.width, self.frame.size.height-tabBarHeight-90) style:UITableViewStylePlain];
    tableResult.delegate = self;
    tableResult.dataSource = self;
    [self addSubview:tableResult];
    
    [self.tableResult setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *tableResultTopConstraint = [NSLayoutConstraint constraintWithItem:self.tableResult attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:leftRightView attribute:NSLayoutAttributeBottom multiplier:1 constant:3];
    NSLayoutConstraint *tableResultLeadingConstraint = [NSLayoutConstraint constraintWithItem:self.tableResult attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *tableResultTrailingConstraint = [NSLayoutConstraint constraintWithItem:self.tableResult attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    NSLayoutConstraint *tableResultBottomConstraint = [NSLayoutConstraint constraintWithItem:self.tableResult attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    [self addConstraints:@[tableResultTopConstraint, tableResultLeadingConstraint, tableResultTrailingConstraint, tableResultBottomConstraint]];
	
}
-(void)clearHistory{
	[self.rstSdy clear];
	[self.rightRecorts removeAllObjects];
	[self.wrongRecorts removeAllObjects];
	[self.tableResult reloadData];

}
-(void)showRightRecords{
	//left.frame=CGRectMake(10, 160, 150, 35);
    //left.frame=CGRectMake(10, 53, windowWidth/2-10, 35);
    self.leftHeightConstraint.constant = 38;
    self.rightHeightConstraint.constant = 35;
    [self setNeedsLayout];
    [self layoutIfNeeded];
//    CGRect leftFrame = left.frame;
//    leftFrame.size.height = 38;
//    left.frame = leftFrame;
//
//    CGRect rightFrame = right.frame;
//    rightFrame.size.height = 35;
//    right.frame = rightFrame;
	[left setBackgroundImage:[UIImage imageNamed:@"blueleftPop.png"] forState:UIControlStateNormal];
	//right.frame=CGRectMake(162, 160, 150, 31);
    //right.frame=CGRectMake(windowWidth/2+2, 53, windowWidth/2-10, 31);
	[right setBackgroundImage:[UIImage imageNamed:@"redright.png"] forState:UIControlStateNormal];
	currentArray=self.rightRecorts;
	[self.tableResult reloadData];
}
-(void)showWrongRecords{
  	//left.frame=CGRectMake(10, 160, 150, 31);
    //left.frame=CGRectMake(10, 53, 150, 31);
    self.leftHeightConstraint.constant = 35;
    self.rightHeightConstraint.constant = 38;
    [self setNeedsLayout];
    [self layoutIfNeeded];
//    CGRect rightFrame = right.frame;
//    rightFrame.size.height = 38;
//    right.frame = rightFrame;
//
//    CGRect leftFrame = left.frame;
//    leftFrame.size.height = 35;
//    left.frame = leftFrame;
	[left setBackgroundImage:[UIImage imageNamed:@"leftblue.png"] forState:UIControlStateNormal];
	//right.frame=CGRectMake(162, 160, 150, 35);
    //right.frame=CGRectMake(windowWidth/2+2, 53, windowWidth/2-10, 35);
	[right setBackgroundImage:[UIImage imageNamed:@"redrightPop.png"] forState:UIControlStateNormal];
    currentArray=self.wrongRecorts;
	[self.tableResult reloadData];

}
-(void)readRecentRecords{
	self.rightRecorts=allRightsGroup;
	self.wrongRecorts=allWrongsGroup;

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 44;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
	
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
	return currentArray!=nil?[currentArray count]:1;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
	
	static NSString *StudyResultTableIdentifier = @"StudyResultTableIdentifier";
    NSInteger index=indexPath.row;
	if (currentArray!=nil) {
		card=[currentArray objectAtIndex:index];
	}else {
		card=[NSMutableDictionary new];
		//card=@"";
	}
	
    UITableViewCell *cell;
	cell= [tableView dequeueReusableCellWithIdentifier:StudyResultTableIdentifier];
	
	if (!cell) {
		//cell=[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:nil];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:StudyResultTableIdentifier];
		cell.textLabel.font=[UIFont systemFontOfSize:14.0];
		//cell.textLabel.textColor=[UIColor whiteColor];
		//cell.backgroundColor=[UIColor grayColor];
        cell.textLabel.numberOfLines = 0;
	}
    cell.textLabel.text=[card objectForKey:@"term"];
	
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSMutableArray *cards=[NSMutableArray arrayWithCapacity:0];
    NSInteger index=indexPath.row;
    card=[currentArray objectAtIndex:index];
    [cards addObject:card];
    DeskTopAppDelegate* delegate=(DeskTopAppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate studyCards:cards];
}

@end
