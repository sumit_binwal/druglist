//
//  Card.h
//  DeskTop
//
//  Created by Acmesoftcn on 1/5/12.
//  Copyright 2012 XiaoLei. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Card : NSObject<NSCoding> {
	NSString *term;
	NSString *definition;
	NSString *studyHistory;
}
@property(nonatomic,strong)NSString *term;
@property(nonatomic,strong)NSString *definition;
@property(nonatomic,strong)NSString *studyHistory;
@end
