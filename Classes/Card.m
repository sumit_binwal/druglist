//
//  Card.m
//  DeskTop
//
//  Created by Acmesoftcn on 1/5/12.
//  Copyright 2012 XiaoLei. All rights reserved.
//

#import "Card.h"


@implementation Card
@synthesize term,definition,studyHistory;
-(id)initWithCoder:(NSCoder*)coder{
  if (self=[super init]) {
	  self.term=[coder decodeObjectForKey:@"term"];
	  self.definition=[coder decodeObjectForKey:@"definition"];
	  self.studyHistory=[coder decodeObjectForKey:@"studyHistory"];
  }
	return self;
}
- (void)encodeWithCoder:(NSCoder *)coder{
	[coder encodeObject:self.term forKey:@"term"];
	[coder encodeObject:self.definition forKey:@"definition"];
	[coder encodeObject:self.studyHistory forKey:@"studyHistory"];
}
-(BOOL)isEqual:(Card*)card{
	return [self.term isEqualToString:card.term];  

}

-(NSString*)description{
	NSString *des=[NSString stringWithFormat:@"%@-%@",self.term,self.studyHistory];
	return des;

}
@end
