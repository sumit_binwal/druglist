//
//  RecentStudy.h
//  DeskTop
//
//  Created by Acmesoftcn on 1/5/12.
//  Copyright 2012 XiaoLei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecentStudy : NSObject{
	NSMutableArray *storeRight;
	NSMutableArray *storeWrong;
	NSMutableDictionary *studycard;//studycard have been studied
	int recordMaxNum;//records max number
}
@property(nonatomic,strong)NSMutableDictionary *studycard;
@property(nonatomic,strong)NSMutableArray *storeRight;
@property(nonatomic,strong)NSMutableArray *storeWrong;
@property(nonatomic)int recordMaxNum;
-(void)addCard:(NSMutableDictionary*)card;
-(id)initWithDefaultRecordNum;
-(void)storeRightRecent;
-(void)storeWrongRecent;
-(void)clear;
@end
