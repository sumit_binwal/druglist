//
//  HomeViewController.m
//  Drug List
//
//  Created by Sumit Sharma on 28/04/19.
//

#import "HomeViewController.h"
#import "DeskTopViewController.h"
#import "DeskTopAppDelegate.h"

@interface HomeViewController ()

@property (nonatomic, weak) IBOutlet UIButton *btnHistory;
@property (weak, nonatomic) IBOutlet UIButton *btnCards;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIButton *btnStartSession;
@property (strong, nonatomic) IBOutlet UIView *tblHeaderView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonTop1Constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cardsTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageAspectRation;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Home";
    // Do any additional setup after loading the view from its nib.
    _btnHistory.layer.cornerRadius = 15;
    _btnCards.layer.cornerRadius = 15;
    _btnStartSession.layer.cornerRadius = 15;
    
    _tblHeaderView.frame = CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height - 64);
    _tblView.tableHeaderView = _tblHeaderView;
}

- (IBAction)showHistory {
    DeskTopViewController *viewCtrl = [[DeskTopViewController alloc] initWithNibName:@"DeskTopViewController" bundle:nil];
    DeskTopAppDelegate *appDelegate = (DeskTopAppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.viewController = viewCtrl;
    [self.navigationController pushViewController: viewCtrl animated:YES];
}
- (IBAction)showCardsScree:(id)sender
{
    DeskTopViewController *viewCtrl = [[DeskTopViewController alloc] initWithNibName:@"DeskTopViewController" bundle:nil];
    
    DeskTopAppDelegate *appDelegate = (DeskTopAppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.viewController = viewCtrl;
    viewCtrl.allcards;
    viewCtrl.cardsTabActivated;
    [self.navigationController pushViewController: viewCtrl animated:YES];

}
- (IBAction)startSessionScreen:(id)sender {
    
    //DeskTopViewController *viewCtrl = [[DeskTopViewController alloc] initWithNibName:@"DeskTopViewController" bundle:nil];
    ShowStudyCards *viewCtrl = [[ShowStudyCards alloc] init];
    
    DeskTopAppDelegate *appDelegate = (DeskTopAppDelegate *)[UIApplication sharedApplication].delegate;
    viewCtrl.cardsgroup = allGroups;
    [viewCtrl.cardTbl reloadData];
    [self.navigationController pushViewController:viewCtrl animated:true];
    
    

}

- (void)updateFramesAsPerSize:(CGSize)size orientation:(UIInterfaceOrientation)orientation {
    
    
    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
        
        self.btnTopConstraint.constant = 10;
        self.buttonTop1Constraint.constant = 10;
        self.cardsTopConstraint.constant = 10;
        self.imageAspectRation.constant = 80;
        self.tblView.scrollEnabled = false;
    } else {
        self.cardsTopConstraint.constant = 50;
        self.btnTopConstraint.constant = 20;
        self.buttonTop1Constraint.constant = 20;
        self.imageAspectRation.constant = 120;
        self.tblView.scrollEnabled = true;
    }
}


- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
         [self updateFramesAsPerSize:size orientation:orientation];
     } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         
     }];
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
