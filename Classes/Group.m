//
//  Group.m
//  DeskTop
//
//  Created by Acmesoftcn on 1/5/12.
//  Copyright 2012 XiaoLei. All rights reserved.
//

#import "Group.h"
#import "Card.h"

@implementation Group
@synthesize groupName,cards;

-(id)initWithName:(NSString*)name{
  if (self=[super init]) {
	  self.groupName=name;
	  self.cards=[[NSMutableArray alloc]initWithCapacity:0];
  }
	return self;
}
-(void)addCard:(NSMutableDictionary*)card{
	[self.cards addObject:card];

}
@end
