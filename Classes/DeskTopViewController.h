//
//  DeskTopViewController.h
//  DeskTop
//
//  Created by Acmesoftcn on 12/30/11.
//  Copyright 2011 XiaoLei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMLParser.h"
#import "RotateTool.h"
#import "Clock.h"
#import "RecentStudy.h"
#import "StudyResult.h"
#import  "Groups.h"
#import "Study.h"
#import "ShowStudyCards.h"
@interface DeskTopViewController : UIViewController<UITabBarDelegate> {
	NSMutableArray *groups;
	UITabBar *tb;
	Study *study;
	StudyResult *studyRst;
	Groups  *grpsV;
	ShowStudyCards *showCards;
    UITabBarItem *itemRnt;
    UITabBarItem *itemGps;
    
    UIButton *btnReview;
    UIButton *AllCards;
}
@property(nonatomic,strong) NSMutableArray *groups;
@property(nonatomic,strong) UITabBar *tb;
@property(nonatomic,strong) Study *study;
@property(nonatomic,strong) StudyResult *studyRst;
@property(nonatomic,strong) Groups  *grpsV;
@property(nonatomic,strong) ShowStudyCards *showCards;
-(void)showCardsOfGroup:(Group*)group;
-(void)studyCards:(NSMutableArray*)cards;
-(void)addUI;
-(void)initSubViews;
-(void)studyAll;
-(void)testXMLParser;
-(void)allcards;
-(void)cardsTabActivated;
@end

