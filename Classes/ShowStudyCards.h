//
//  ChooseStudyCard.h
//  Parmedic Drug List
//
//  Created by Acmesoftcn on 1/7/12.
//  Copyright 2012 XiaoLei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecentStudy.h"
@interface ShowStudyCards : UIViewController<UITableViewDelegate,UITableViewDataSource,UIPickerViewDataSource,UIPickerViewDelegate> {
	NSMutableArray *cards;
	NSMutableArray *cardsgroup;
	UITableView *cardTbl;
	NSMutableDictionary *card;
	int cardNum;
	NSString *cardType;
	RecentStudy *history;
    UIPickerView *pickV;
}
@property(nonatomic,strong)NSMutableArray *cards;
@property(nonatomic,strong)NSMutableArray *cardsgroup;
@property(nonatomic,strong)NSMutableArray *shuffledCardsgroup;
@property(nonatomic,assign) BOOL isShuffled;
@property(nonatomic,strong)UITableView *cardTbl;
@property(nonatomic,strong)NSMutableArray *rightCards,*wrongCards;
@property(nonatomic,strong)RecentStudy *history;
@property(nonatomic,strong)UIPickerView *pickV;
-(void)pickWrongCards;
-(void)pickCardsNum:(int)num;
-(void)pickAllCards;
- (NSMutableArray *)randArray:(NSMutableArray *)ary;
@end
